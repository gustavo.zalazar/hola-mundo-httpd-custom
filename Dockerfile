FROM alpine:latest
RUN apk add apache2
RUN apk --no-cache add --update                                         \
        --repository http://dl-3.alpinelinux.org/alpine/edge/testing/   \
        dos2unix
WORKDIR /tmp
COPY docker-entrypoint.sh /tmp/
RUN dos2unix /tmp/docker-entrypoint.sh
RUN chmod +x /tmp/docker-entrypoint.sh
ENTRYPOINT ["/tmp/docker-entrypoint.sh"]
CMD ["Hola mundo"]
