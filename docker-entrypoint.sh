#!/bin/sh
set -e
exec /bin/sh -c "echo '$1' > /var/www/localhost/htdocs/index.html && httpd -D FOREGROUND"
exec "$@"