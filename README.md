# Docker image "Hola mundo"

## clonar

```sh
mkdir -p Workspace/afip/
cd Workspace/afip/
git clone https://gitlab.com/gustavo.zalazar/hola-mundo-httpd-custom.git
```

## build

```sh
cd Workspace/afip/hola-mundo-httpd-custom
```

## execute

```sh
docker run -dit -p80:80 mikroways/hola‑mundo‑httd-custom "Este es un mensaje especial"
```

## test

curl http://localhost/